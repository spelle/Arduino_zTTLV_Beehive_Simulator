/*
 * Arduino_zTTLV_Beehive_Simulator.cpp
 *
 *  Created on: Jan 22, 2015
 *      Author: sherpa
 */

#include "Arduino_zTTLV_Beehive_Simulator.h"
#include "Arduino_Simu_Sine.h"

#define BLINK_LED_PERIOD 1000
#define SINE_PERIOD 1000

void setup()
{
	// Set PB5 as OUTPUT
	DDRB |= (1 << PB5 ) ;

	Serial.begin(115200) ;

#ifdef DEBUG
	// fill in the UART file descriptor with pointer to writer.
	fdev_setup_stream (&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE) ;

	// The uart is the standard output device STDOUT.
	stdout = &uartout ;
#endif

	PDEBUG("--- Arduino zTTLV Beehive Simulator ---\r\n");
}

void loop()
{
	//TODO : Read CollectorID from EEPROM

	static uint32_t ulCurrentTime = 0 ;
	static uint32_t ulNextTimeLED = BLINK_LED_PERIOD ;
	static uint32_t ulNextTimeSine = SINE_PERIOD ;

	// zTTLV Buffer
	static uint8_t ui8Array[80] ;
	static zTTLV_Buffer_t zTTLV_Buffer = { ui8Array } ;

	// zTTLV Buffer for Beehives
	static uint8_t ui8ArrayBeehives[20] ;
	static zTTLV_Buffer_t zTTLV_Buffer_Beehives = { ui8ArrayBeehives } ;

	// Temp variables for storing zTTLV Items values
	zTTLV_Item_t zTTLV_Item ;
	uint8_t wUInt8 ;
	uint16_t wUInt16 ;

	// Initialize the Buffer
	zTTLV_Buffer_Initialize( & zTTLV_Buffer, sizeof(ui8Array) ) ;
	zTTLV_Buffer_Initialize( & zTTLV_Buffer_Beehives, sizeof(ui8ArrayBeehives) ) ;

	// Get the current time
	ulCurrentTime = millis() ;

	// Simulation Sine
	if( ulCurrentTime > ulNextTimeSine )
	{
		unsigned int uiSineVal = getSineValue() ;
		PDEBUG( "%lu : uiSineVal : %u (0x%.4X)\r\n", ulCurrentTime, uiSineVal, uiSineVal ) ;

		// Build the message
		wUInt8 = 100 ; // MsgID is 100 for Request
		zTTLV_Item_Initialize( &zTTLV_Item, MsgID, UINT8, sizeof(uint8_t), &wUInt8 ) ;
		zTTLV_Buffer_Put( & zTTLV_Buffer, zTTLV_Item ) ;
		wUInt16 = 0xA5A5 ; // CollectorID is 0xA5A5 for testing purpose
		zTTLV_Item_Initialize( &zTTLV_Item, CollectorID, UINT16, sizeof(uint16_t), &wUInt16 ) ;
		zTTLV_Buffer_Put( & zTTLV_Buffer, zTTLV_Item ) ;

		wUInt16 = 0x5A5A ; // BeehiveID is 0x5A5A for testing purpose
		zTTLV_Item_Initialize( &zTTLV_Item, BeehiveID, UINT16, sizeof(uint16_t), &wUInt16 ) ;
		zTTLV_Buffer_Put( & zTTLV_Buffer_Beehives, zTTLV_Item ) ;

		zTTLV_Buffer_Put( & zTTLV_Buffer, zTTLV_Item ) ;

		for ( uint8_t ui8 = 0 ; zTTLV_Buffer.pzTTLV_Buffer[0] >= ui8 && sizeof(ui8Array) > ui8 ; ui8 ++ )
		{
#if !defined(DEBUG)
			Serial.write( ui8Array[ui8] ) ;
#endif // defined(DEBUG)
			PDEBUG( "0x%.2X ", ui8Array[ui8] ) ;
		}
		PDEBUG( "\r\n" ) ;

		zTTLV_Buffer_Initialize( & zTTLV_Buffer, sizeof(ui8Array) ) ;

		ulNextTimeSine = ulNextTimeSine + SINE_PERIOD ;
	}

	// Kind of watchdog. Lighting the D13 LED.
	if( ulCurrentTime > ulNextTimeLED )
	{
		PORTB ^= (1 << PB5 ) ;

		ulNextTimeLED = ulCurrentTime + BLINK_LED_PERIOD ;
	}
}
